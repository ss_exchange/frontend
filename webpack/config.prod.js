const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");

module.exports = () => ({
  output: {
    filename: "js/[name].[contenthash].js"
  },
  mode: "production",
  performance: { hints: false },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: { sourceMap: true }
          },
          { loader: "sass-loader" }
        ]
      },
      {
        use: [
          {
            loader: "babel-loader",
            options: {
              plugins: ["lodash"],
              presets: [["env", { modules: false, targets: { node: 10 } }]]
            }
          }
        ],
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].[hash].css",
      chunkFilename: "[id].[hash].css"
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  ]
});
