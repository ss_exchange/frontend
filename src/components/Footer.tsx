import React from "react";

import { Typography, Link, Container, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),

    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6)
    }
  }
}));

export const Footer: React.FC = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="md" component="footer" className={classes.footer}>
      <Typography variant="body2" color="textSecondary" align="center">
        Copyright ©{" "}
        <Link color="inherit" href="mailto:piotrgrundas@gmail.com">
          piotrgrundas@gmail.com
        </Link>{" "}
        {new Date().getFullYear()}
        {"."}
      </Typography>
    </Container>
  );
};
