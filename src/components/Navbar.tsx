import React from "react";
import { Link as RouterLink } from "react-router-dom";

import { makeStyles, AppBar, Toolbar, Link } from "@material-ui/core";
import { Assessment, Search } from "@material-ui/icons";

import { searchUrl, storedQuotesUrl } from "@urls";
import { useIsXsDown } from "@hooks";

const useStyles = makeStyles(theme => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  toolbar: {
    flexWrap: "wrap",
    justifyContent: "flex-end"
  },
  toolbarTitle: {
    flexGrow: 1
  },
  link: {
    margin: theme.spacing(1, 1.5),
    "& svg": {
      margin: "0 5px -7px 15px"
    }
  }
}));

export const Navbar: React.FC = () => {
  const classes = useStyles();
  const isXs = useIsXsDown();

  return (
    <AppBar
      position="static"
      color="default"
      elevation={0}
      className={classes.appBar}
    >
      <Toolbar className={classes.toolbar}>
        <nav>
          <Link
            variant="button"
            color="textPrimary"
            className={classes.link}
            component={RouterLink}
            to={searchUrl}
          >
            <Search />
            {!isXs && "Search"}
          </Link>
          <Link
            variant="button"
            color="textPrimary"
            href="#"
            className={classes.link}
            component={RouterLink}
            to={storedQuotesUrl}
          >
            <Assessment />
            {!isXs && "Stored data"}
          </Link>
        </nav>
      </Toolbar>
    </AppBar>
  );
};
