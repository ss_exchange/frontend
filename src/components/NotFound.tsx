import React from "react";
import { Typography } from "@material-ui/core";

export const NotFound: React.FC<{ message?: string }> = ({
  message = "Not Found"
}) => (
  <Typography align="center" variant="button" display="block">
    {message}
  </Typography>
);
