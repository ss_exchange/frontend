import React from "react";
import { CssBaseline, makeStyles, Container } from "@material-ui/core";

import { Navbar } from "./Navbar";
import { Footer } from "./Footer";

const useStyles = makeStyles(theme => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none"
    },
    "#root": {
      display: "flex",
      flexDirection: "column",
      minHeight: "100vh",
      overflow: "hidden",
      position: "relative"
    }
  },
  main: {
    flex: "1 0 auto",
    padding: "2rem 0",
    [theme.breakpoints.down("sm")]: {
      paddingRight: theme.spacing(2),
      paddingLeft: theme.spacing(2)
    }
  }
}));

export const Layout: React.FC = ({ children }) => {
  const classes = useStyles();

  return (
    <>
      <CssBaseline />
      <Navbar />
      <Container maxWidth="md" component="main" className={classes.main}>
        {children}
      </Container>
      <Footer />
    </>
  );
};
