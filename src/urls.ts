export const searchUrl = "/";
export const companyDetailsUrl = "/company/:symbol/";
export const storedQuotesUrl = "/stored-quotes/";
