import "typeface-roboto";
import React from "react";
import { render } from "react-dom";
import { hot } from "react-hot-loader";
import { BrowserRouter as Router } from "react-router-dom";
import { LastLocationProvider } from "react-router-last-location";

import { AppRouter } from "./router";
import { Layout } from "@components";
import { configureAxios } from "@core";

configureAxios();

const startApp = () => {
  const Root = hot(module)(() => {
    return (
      <Router>
        <LastLocationProvider>
          <Layout>
            <AppRouter />
          </Layout>
        </LastLocationProvider>
      </Router>
    );
  });

  render(<Root />, document.getElementById("root"));

  // Hot Module Replacement API
  if (module.hot) {
    module.hot.accept();
  }
};

startApp();
