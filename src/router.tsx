import { Switch, Route } from "react-router-dom";
import React from "react";

import { searchUrl, storedQuotesUrl, companyDetailsUrl } from "@urls";
import {
  SearchView,
  StoredQuotesView,
  CompanyDetailsView,
  NotFoundView
} from "@views";

export const AppRouter = () => (
  <Switch>
    <Route exact path={searchUrl} component={SearchView} />
    <Route path={storedQuotesUrl} component={StoredQuotesView} />
    <Route path={companyDetailsUrl} component={CompanyDetailsView} />
    <Route path="*" component={NotFoundView} />
  </Switch>
);
