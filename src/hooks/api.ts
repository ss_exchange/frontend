import { makeUseAxios } from "axios-hooks";
import axios from "axios";

import { API_URL } from "@core";

export const useAxios = makeUseAxios({
  axios: axios.create({ baseURL: API_URL })
});
