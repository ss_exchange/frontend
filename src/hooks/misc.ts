import { useState, useRef, useEffect } from "react";

export const useToggle = (initial = false): [boolean, () => void] => {
  const [toggle, setToggle] = useState(initial);
  const toggler = () => setToggle(p => !p);
  return [toggle, toggler];
};

export const useInterval = (callback: (...p: any) => any, delay: any) => {
  const savedCallback = useRef(callback);

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    const tick = () => savedCallback.current();

    let id = setInterval(tick, delay);
    return () => clearInterval(id);
  }, [delay]);
};
