import { useMediaQuery, useTheme } from "@material-ui/core";

export const useIsSmDown = () => {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.down("sm"));
};

export const useIsXsDown = () => {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.down("xs"));
};
