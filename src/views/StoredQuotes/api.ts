import { ICompanyLastTradingDayTrack } from "../CompanyDetails/api";
import { TUseAxios, companyLastTradingDayTrackApi } from "@core";
import { useAxios } from "@hooks";

export type TCompanyLastTradingDayTrackResult =
  | ICompanyLastTradingDayTrack[]
  | undefined;

export const useCompanyLastTradingDayApi = (): TUseAxios<ICompanyLastTradingDayTrack[]> =>
  useAxios<ICompanyLastTradingDayTrack[]>(companyLastTradingDayTrackApi());
