import React from "react";
import { RouteComponentProps } from "react-router-dom";
import {
  Box,
  List,
  ListItem,
  Typography,
  ListSubheader,
  Divider
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";

import { useCompanyLastTradingDayApi } from "./api";

export const StoredQuotesView: React.FC<RouteComponentProps> = () => {
  const [{ data, loading }] = useCompanyLastTradingDayApi();

  return loading ? (
    <Box textAlign="center">
      <Skeleton animation="wave" />
      <Skeleton animation="wave" />
      <Skeleton animation="wave" />
    </Box>
  ) : data.length ? (
    <>
      {data.map(({ symbol, quotes }) => (
        <List
          key={symbol}
          subheader={
            <ListSubheader>
              <Typography variant="h4">{symbol}</Typography>
            </ListSubheader>
          }
        >
          {quotes.map(({ id, ...data }) => (
            <React.Fragment key={id}>
              <Divider />
              {Object.entries(data).map(([key, value]) => (
                <ListItem key={key}>
                  <Typography variant="subtitle1" display="block">
                    {key} - {value}
                  </Typography>
                </ListItem>
              ))}
            </React.Fragment>
          ))}
        </List>
      ))}
    </>
  ) : (
    <Typography align="center" variant="h4" display="block">
      No stored quotes
    </Typography>
  );
};
