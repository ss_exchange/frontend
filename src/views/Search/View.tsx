import React, { useState, useEffect, useRef } from "react";
import { debounce } from "lodash";
import { useQueryParam, StringParam } from "use-query-params";
import { RouteComponentProps } from "react-router-dom";

import { SearchForm, CompanyList } from "./components";
import { useCompanySearchApi } from "./api";

export const SearchView: React.FC<RouteComponentProps> = () => {
  const [querySymbol, setQuerySymbol] = useQueryParam("symbol", StringParam);
  const [symbol, setSymbol] = useState(querySymbol || "");
  const [{ data, loading }, executeSearch] = useCompanySearchApi();
  const debouncedSearch = useRef(
    debounce(
      (symbol: string | undefined) => {
        setQuerySymbol(symbol);

        if (symbol) {
          executeSearch(symbol);
        }
      },
      250,
      { trailing: true }
    )
  );

  useEffect(() => debouncedSearch.current(symbol), [symbol]);

  return (
    <>
      <SearchForm loading={loading} symbol={symbol} onChange={setSymbol} />
      <br />
      <CompanyList data={symbol ? data : []} />
    </>
  );
};
