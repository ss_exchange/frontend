import React from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableHead,
  Link
} from "@material-ui/core";
import { ContactMail } from "@material-ui/icons";
import { Link as RouterLink, generatePath } from "react-router-dom";

import { TCompanySearchResult } from "../api";
import { companyDetailsUrl } from "@urls";

export const CompanyList: React.FC<{ data: TCompanySearchResult }> = ({
  data
}) =>
  !data?.length ? null : (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Symbol</TableCell>
            <TableCell>Name</TableCell>
            <TableCell align="center">Details</TableCell>
          </TableRow>
        </TableHead>
        <TableBody
          onClick={evt => {
            evt.preventDefault();
          }}
        >
          {data.map(({ name, symbol }) => (
            <TableRow hover key={symbol}>
              <TableCell>{symbol}</TableCell>
              <TableCell>{name}</TableCell>
              <TableCell align="center">
                <Link
                  variant="button"
                  color="textPrimary"
                  component={RouterLink}
                  to={generatePath(companyDetailsUrl, { symbol })}
                >
                  <ContactMail />
                </Link>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
