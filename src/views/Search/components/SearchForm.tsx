import React from "react";
import {
  Input,
  Grid,
  FormControl,
  InputAdornment,
  CircularProgress,
  colors
} from "@material-ui/core";
import { Search } from "@material-ui/icons";

import { TSetStateHook } from "@core";

interface Iprops {
  symbol: string;
  loading: boolean;
  onChange: TSetStateHook<string>;
}

export const SearchForm: React.FC<Iprops> = ({ symbol, onChange, loading }) => (
  <form>
    <Grid container>
      <Grid item xs={12}>
        <FormControl fullWidth>
          <Input
            disabled={loading}
            placeholder="Search for company..."
            id="search-company"
            value={symbol}
            onChange={(evt: React.ChangeEvent<HTMLInputElement>) =>
              onChange(evt.target.value)
            }
            startAdornment={
              <InputAdornment position="start">
                {loading ? (
                  <CircularProgress
                    size={21}
                    style={{ marginRight: 3, color: colors.grey[500] }}
                  />
                ) : (
                  <Search />
                )}
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
    </Grid>
  </form>
);
