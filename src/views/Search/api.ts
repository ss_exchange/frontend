import { ResponseValues } from "axios-hooks";
import { AxiosPromise } from "axios";

import { useAxios } from "@hooks";
import { companyListApi, ICompany } from "@core";

export type TCompanySearchResult = ICompany[] | undefined;

export const useCompanySearchApi = (): [
  ResponseValues<TCompanySearchResult>,
  (symbol: string) => AxiosPromise<TCompanySearchResult>
] => {
  const [responseValues, refetch] = useAxios<TCompanySearchResult>(
    { url: undefined },
    { manual: true }
  );
  const search = (symbol: string) => refetch({ url: companyListApi(symbol) });
  return [responseValues, search];
};
