import React from "react";
import { RouteComponentProps } from "react-router-dom";

import { NotFound } from "@components";

export const NotFoundView: React.FC<RouteComponentProps> = () => <NotFound />;
