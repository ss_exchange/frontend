import React, { useState, useEffect } from "react";

import {
  CardMedia,
  Grid,
  Typography,
  Badge,
  makeStyles,
  ListItem,
  List,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Divider,
  Button
} from "@material-ui/core";
import {
  Image,
  AlarmOn,
  AlarmOff,
  Translate,
  AttachMoney,
  Domain,
  SaveAlt,
  Sync,
  Equalizer,
  Clear
} from "@material-ui/icons";

import { ICompany } from "@core";
import {
  TCompanyLogoDetailsResult,
  TCompanyLatestTradingDayResult,
  useCompanyLastTradingDayTrackApi
} from "../api";
import { useInterval } from "@hooks";
import classNames from "classnames";

const REFRESH_TIMEOUT = 1000 * 30;

const useStyles = makeStyles(theme => ({
  "@keyframes spin": {
    from: { transform: "rotate(0deg)" },
    to: { transform: "rotate(180deg)" }
  },
  placeholder: {
    height: "100%",
    width: "100%",
    color: theme.palette.grey[400]
  },
  status: {
    alignSelf: "center",
    textAlign: "center"
  },
  progress: {
    backgroundColor: theme.palette.success.main
  },
  progressIndicator: {
    "& svg": {
      animation: "$spin 1s linear"
    }
  }
}));

interface IProps {
  companyDetails: ICompany;
  companyLogoDetails: TCompanyLogoDetailsResult;
  companyLatestTradingDay: TCompanyLatestTradingDayResult;
  refetchCompanyDetails(): void;
}

export const CompanyCard: React.FC<IProps> = ({
  companyDetails: { name, market_open, market_close, symbol, currency },
  companyLogoDetails,
  refetchCompanyDetails,
  companyLatestTradingDay
}) => {
  const [
    { data: trackData, loading: trackLoading }
  ] = useCompanyLastTradingDayTrackApi(symbol, false);
  const [
    { data: updateTrackData, loading: updateTrackLoading },
    updateTracking
  ] = useCompanyLastTradingDayTrackApi(symbol, true);
  const [isSessionInProgress, setIsSessionInProgress] = useState(
    checkIfSessionIsInProgress(market_open, market_close)
  );

  const [tracking, setTracking] = useState(false);
  const [rerenderTimestamp, setRerenderTimestamp] = useState(
    new Date().getTime()
  );
  const classes = useStyles();
  const logo = companyLogoDetails?.logo;

  const info = computeInfoData(
    companyLogoDetails?.domain,
    symbol,
    currency,
    market_open,
    market_close,
    companyLatestTradingDay
  );

  const handleTracking = (
    evt: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    evt.preventDefault();
    updateTracking({
      data: { symbol },
      method: tracking ? "DELETE" : "POST"
    });
  };

  useInterval(() => {
    const inProgress = checkIfSessionIsInProgress(market_open, market_close);

    if (inProgress) {
      refetchCompanyDetails();
      setRerenderTimestamp(new Date().getTime());
    }

    if (isSessionInProgress !== inProgress) {
      setIsSessionInProgress(inProgress);
    }
  }, REFRESH_TIMEOUT);

  useEffect(() => setTracking(!!updateTrackData), [updateTrackData]);
  useEffect(() => setTracking(!!trackData), [trackData]);

  return (
    <Grid container spacing={5}>
      <Grid item xs={12} sm={3} md={2}>
        {logo ? (
          <CardMedia component="img" image={logo} alt={name} />
        ) : (
          <Image className={classes.placeholder} />
        )}
      </Grid>
      <Grid item xs={12} sm={7} md={9} style={{ alignSelf: "center" }}>
        <Typography align="center" variant="h2" display="block">
          {name}
        </Typography>
      </Grid>
      <Grid item xs={12} sm={2} md={1} className={classes.status}>
        {isSessionInProgress && (
          <Badge
            key={`${rerenderTimestamp}`}
            badgeContent="sync"
            color="primary"
            className={classes.progressIndicator}
          >
            <Sync />
          </Badge>
        )}
      </Grid>
      <Grid item xs={12}>
        <List>
          {info.map(
            ({ icon, primaryText, secondaryText, successAvatar }, i) => (
              <React.Fragment key={i}>
                {i < info.length && <Divider variant="inset" component="li" />}
                <ListItem>
                  <ListItemAvatar>
                    <Avatar
                      className={classNames({
                        [classes.progress]: successAvatar && isSessionInProgress
                      })}
                    >
                      {icon}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={primaryText}
                    secondary={secondaryText}
                  />
                </ListItem>
              </React.Fragment>
            )
          )}
        </List>
      </Grid>
      <Grid item xs={12}>
        <Button
          disabled={trackLoading || updateTrackLoading}
          variant="outlined"
          endIcon={tracking ? <Clear /> : <SaveAlt />}
          onClick={handleTracking}
        >
          {tracking
            ? "Stop storring closing quote daily"
            : "Store closing quote daily"}
        </Button>
      </Grid>
    </Grid>
  );
};

const checkIfSessionIsInProgress = (
  marketOpen: string,
  marketClose: string
) => {
  const parseTime = (time: string) => time.split(":").map(v => parseInt(v));
  const [openHours, openMinutes] = parseTime(marketOpen);
  const [closeHours, closeMinutes] = parseTime(marketClose);
  const start = openHours * 60 + openMinutes;
  const close = closeHours * 60 + closeMinutes;
  const date = new Date();
  const now = date.getHours() * 60 + date.getMinutes();
  return start <= now && now <= close;
};

const computeInfoData = (
  domain: string | undefined,
  symbol: string,
  currency: string,
  market_open: string,
  market_close: string,
  companyLatestTradingDay: TCompanyLatestTradingDayResult
) => [
  ...(domain
    ? [
        {
          primaryText: "Website",
          secondaryText: (
            <a href={`https://${domain}`} target="_blank">
              {domain}
            </a>
          ),
          icon: <Domain />
        }
      ]
    : []),
  ...[
    {
      primaryText: "Symbol",
      secondaryText: symbol,
      icon: <Translate />
    },
    {
      primaryText: "Currency",
      secondaryText: currency,
      icon: <AttachMoney />
    },
    {
      primaryText: "Market open",
      secondaryText: market_open,
      successAvatar: true,
      icon: <AlarmOn />
    },
    {
      primaryText: "Market close",
      secondaryText: market_close,
      successAvatar: true,
      icon: <AlarmOff />
    }
  ],
  ...(companyLatestTradingDay
    ? [
        {
          primaryText: "Last trading day quote",
          secondaryText: (
            <>
              {Object.entries(companyLatestTradingDay).map(([key, value]) => (
                <React.Fragment key={key}>
                  {key} - {value}
                  <br />
                </React.Fragment>
              ))}
            </>
          ),
          icon: <Equalizer />
        }
      ]
    : [])
];
