import React, { useEffect } from "react";
import { RouteComponentProps } from "react-router-dom";

import {
  useCompanyLogoDetailsApi,
  useCompanyDetailsApi,
  useCompanyLastTradingDayApi
} from "./api";
import { NotFound } from "@components";
import { Box } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import { CompanyCard } from "./components";

export const CompanyDetailsView: React.FC<RouteComponentProps<{
  symbol: string;
}>> = ({
  match: {
    params: { symbol }
  }
}) => {
  const [
    { data: companyLogoDetailsData, error: companyLogoDetailsError },
    fetchLogoDetails
  ] = useCompanyLogoDetailsApi();
  const [
    { data: comapnyDetailsData, loading, error },
    refetchCompanyDetails
  ] = useCompanyDetailsApi(symbol);
  const [{ data: companyLatestTradingDayData }] = useCompanyLastTradingDayApi(
    symbol
  );

  useEffect(() => {
    if (
      comapnyDetailsData &&
      !companyLogoDetailsData &&
      !companyLogoDetailsError
    ) {
      fetchLogoDetails(comapnyDetailsData.name);
    }
  }, [comapnyDetailsData]);

  if (!comapnyDetailsData && loading) {
    return (
      <Box textAlign="center">
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
        <Skeleton animation="wave" />
      </Box>
    );
  }

  return error ? (
    <NotFound />
  ) : (
    <CompanyCard
      companyDetails={comapnyDetailsData}
      companyLogoDetails={companyLogoDetailsData}
      refetchCompanyDetails={refetchCompanyDetails}
      companyLatestTradingDay={companyLatestTradingDayData}
    />
  );
};
