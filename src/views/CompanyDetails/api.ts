import { useAxios } from "@hooks";
import {
  companyLogoDetailsApi,
  ICompany,
  companyDetailsApi,
  TUseAxios,
  companyLastTradingDayTrackApi,
  companyLastTradingDayApi
} from "@core";
import { ResponseValues } from "axios-hooks";
import { AxiosPromise } from "axios";

export interface ICompanyLogoDetails {
  logo: string;
  domain: string;
}

export type TCompanyLogoDetailsResult = ICompanyLogoDetails | undefined;

export const useCompanyLogoDetailsApi = (): [
  ResponseValues<TCompanyLogoDetailsResult>,
  (name: string) => AxiosPromise<TCompanyLogoDetailsResult>
] => {
  const [responseValues, refetch] = useAxios<ICompanyLogoDetails>(
    { url: undefined },
    { manual: true }
  );
  const fetch = (name: string) => refetch({ url: companyLogoDetailsApi(name) });
  return [responseValues, fetch];
};

export const useCompanyDetailsApi = (symbol: string): TUseAxios<ICompany> =>
  useAxios<ICompany>(companyDetailsApi(symbol));

export interface ICompanyLatestTradingDay {
  date: string;
  open: string;
  high: string;
  low: string;
  close: string;
  volume: string;
}

export type TCompanyLatestTradingDayResult =
  | ICompanyLatestTradingDay
  | undefined;

export const useCompanyLastTradingDayApi = (
  symbol: string
): TUseAxios<ICompanyLatestTradingDay> =>
  useAxios<ICompanyLatestTradingDay>(companyLastTradingDayApi(symbol));

export interface ICompanyLastTradingDayTrack {
  symbol: string;
  id: number;
  quotes: (ICompanyLatestTradingDay & { id: number })[];
}

export const useCompanyLastTradingDayTrackApi = (
  symbol: string,
  manual = true
): TUseAxios<ICompanyLastTradingDayTrack> =>
  useAxios<ICompanyLastTradingDayTrack>(
    {
      url: companyLastTradingDayTrackApi(symbol)
    },
    { manual }
  );
