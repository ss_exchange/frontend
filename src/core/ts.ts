import { ResponseValues, RefetchOptions } from "axios-hooks";
import { AxiosRequestConfig, AxiosPromise } from "axios";

export type TSetStateHook<T> = React.Dispatch<React.SetStateAction<T>>;

export type TUseAxios<T> = [
  ResponseValues<T>,
  (config?: AxiosRequestConfig, options?: RefetchOptions) => AxiosPromise<T>
];
