export interface ICompany {
  symbol: string;
  name: string;
  type: string;
  region: string;
  market_open: string;
  market_close: string;
  timezone: string;
  currency: string;
  match_score: string;
}
