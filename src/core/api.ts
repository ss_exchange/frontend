import Axios from "axios";
import { configure } from "axios-hooks";
import LRU from "lru-cache";

import { API_URL } from "./config";

export const companyListApi = (symbol: string) => `company/search/${symbol}/`;
export const companyLogoDetailsApi = (symbol: string) =>
  `company/logo/${symbol}/`;
export const companyDetailsApi = (symbol: string) => `company/${symbol}/`;
export const companyLastTradingDayApi = (symbol: string) =>
  `company/last_trading_day/${symbol}/`;
export const companyLastTradingDayTrackApi = (symbol?: string) =>
  symbol
    ? `company/last_trading_day/track/${symbol}/`
    : `company/last_trading_day/track/`;

export const axios = Axios.create({
  baseURL: API_URL
});

const cache = new LRU({ max: 10 });

export const configureAxios = () => configure({ axios, cache });
