# Frontend

## Requirements

- node >= 10.0.0 < 11

## Development

    $ npm i
    $ npm start

App runs under [http://127.0.0.1:3000/](http://127.0.0.1:3000/)
